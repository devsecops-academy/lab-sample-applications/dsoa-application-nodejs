# Running Locally

```
docker run -it --rm --mount type=bind,source="$(pwd)",target=/app -p 8018:8018 --link db node:alpine /bin/sh
```

```
cd /app
npm install
npm start
```