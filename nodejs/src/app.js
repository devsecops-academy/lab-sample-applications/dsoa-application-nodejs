const express = require('express')
const mariadb = require('mariadb');

const port = 8011
const app = express()
const pool = mariadb.createPool({
  host: 'db',
  user: '82618201',
  password: 'animism-sort-galvanic-angina-camp',
  database: 'demo'
});

app.set('view engine', 'pug')

app.get('/app', async (req, res) => {
  conn = await pool.getConnection();
  const rows = await conn.query("SELECT * FROM customer;");
  res.render('index', { customers: rows });
});

app.listen(port, () => console.log(`Listening on port ${port}!`))
